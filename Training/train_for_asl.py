#!/bin/python3
"""
FILENAME :      train_for_asl.py

DESCRIPTION : Training code to generate a CNN+LTSM based on video data stored in a directoy.
                All videos will be named based on their parent dir I.e workdir/name/file1.mp4 will be associated to name.


Usage:
                        python3 train_for_asl.py


AUTHOR :   Network Silence        START DATE :    16 May 2021


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import os

import cv2
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, Flatten, BatchNormalization, Activation
from sklearn.preprocessing import LabelBinarizer
from keras import regularizers
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import Conv2D, MaxPooling2D

from Lib.VideoCapture import VideoGet
from Lib import Utilities as AslUtil


def main():
    """
    Main runner, responsible for controlling the flow of logic
    :return:
    """
    training_data_path = "/media/null/Backup/test/"
    model_filename = "/media/null/Repos/GitLab/ASL_Translator/trained_model_asl.hdf5"
    label_filename = "/media/null/Repos/GitLab/ASL_Translator/model_labels.dat"

    label_list = os.listdir(training_data_path)  # All possible Labels
    max_image_size = 120  # Size we hand to train video_frame at ( Too large and you'll run out of VRAM)
    frame_cap = 12 # Frames from the video we want to train against. I.e all videos need to have the same framecount

    x_train, y_train = build_training_shape(
        training_data_path, frame_cap, label_list, max_image_size
    )
    AslUtil.save_to_file(label_filename, y_train)
    _ = build_nn(
        x_train, y_train, frame_cap, label_list, max_image_size, model_filename
    )


def build_training_shape(data_path, max_frames, class_list, image_size):
    """
    Turn all data into numpy arrays to be used during training.
    :param data_path: Path to data we want to train with
    :param max_frames: Max amount of frames from each video we want to train with
    :param class_list: List of all the possible directories i.e words that have sign representation
    :param image_size: The dimensions of the frame we are using to train
    :return:
    """
    cur_video = 0
    train_image = []  # Image list of all
    train_label = []  # Store Label for each

    for folder in os.listdir(data_path):  # Loop through Folders in asl Dir
        cur_video += 1
        for video in os.listdir(f"{data_path}{folder}"):
            try:
                cap = VideoGet(f"{data_path}{folder}/{video}")
                image_list = []
                flipped_image_list = []
                frame_count = 0
                while not cap.stopped and frame_count < max_frames: # Keep reading the frame from the video unless we have our required frame_cap
                    image = cap.frame
                    frame_count += 1
                    image_list.append(AslUtil.image_processor(image, image_size))
                    flipped_image_list.append(
                        AslUtil.image_processor(cv2.flip(image, 1), image_size)
                    )
                cap.stop()
            except Exception:
                print(f"Bugged File: {folder}/{video}")
                continue
            while frame_count < max_frames:  # Re-add the last frame from the video until we hit the required frame_cap
                frame_count += 1
                image_list.append(image_list[-1])
                flipped_image_list.append(flipped_image_list[-1])
            label_index = class_list.index(folder) # Get the Name of the Sign shown
            train_label.extend([label_index, label_index]) # Add Name to Name
            train_image.extend(image_list)
            train_image.extend(flipped_image_list)
        print(f"Word Finished: {folder}")
        print(f"Words Remaining: {len(class_list) - cur_video}")
    x_train = np.array(train_image).reshape(-1, max_frames, image_size, image_size, 3)
    y_train = np.array(train_label)
    lb = LabelBinarizer().fit(y_train)
    y_train = lb.transform(y_train)

    return x_train, y_train


def add_default_block(half_model, kernel_filters, init, regulizer_lambda):
    """
    Train default blocks 2-5 of the CNN.
    :param half_model: Model currently being built I.e has the first layer on it
    :param kernel_filters: Kernel Size
    :param init: Initializer for the kernel
    :param regulizer_lambda: L2 Regualizer
    :return: Model with newly added block
    """
    half_model.add(
        TimeDistributed(
            Conv2D(
                kernel_filters,
                (3, 3),
                padding="same",
                kernel_initializer=init,
                kernel_regularizer=regularizers.l2(l=regulizer_lambda),
            )
        )
    )
    half_model.add(TimeDistributed(BatchNormalization()))
    half_model.add(TimeDistributed(Activation("relu")))
    half_model.add(
        TimeDistributed(
            Conv2D(
                kernel_filters,
                (3, 3),
                padding="same",
                kernel_initializer=init,
                kernel_regularizer=regularizers.l2(l=regulizer_lambda),
            )
        )
    )
    half_model.add(TimeDistributed(BatchNormalization()))
    half_model.add(TimeDistributed(Activation("relu")))
    half_model.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))
    return half_model


def build_nn(x_train, y_train, max_frames, class_list, image_size, model_filename):
    """
    Built a CNN+LSTM neural network 
    :param x_train: Numpy array of images to be added to the neural network
    :param y_train: One-Hot encoded class list (All possible Sign words)
    :param max_frames: Amount of Frames we took per video
    :param class_list: String array class List
    :param image_size: frame dimension
    :param model_filename: The location to store the trained model
    :return: Return the Model
    """
    initializer = "glorot_uniform"
    reg_lambda = 0.001

    model = Sequential()
    model.add(
        TimeDistributed(
            Conv2D(
                32,
                (7, 7),
                strides=(2, 2),
                padding="valid",
                kernel_initializer=initializer,
                kernel_regularizer=regularizers.l2(l=reg_lambda),
            ),
            input_shape=(max_frames, image_size, image_size, 3),
        )
    )
    model.add(TimeDistributed(BatchNormalization()))
    model.add(TimeDistributed(Activation("relu")))
    model.add(
        TimeDistributed(
            Conv2D(
                32,
                (3, 3),
                kernel_initializer=initializer,
                kernel_regularizer=regularizers.l2(l=reg_lambda),
            )
        )
    )
    model.add(TimeDistributed(BatchNormalization()))
    model.add(TimeDistributed(Activation("relu")))
    model.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

    # 2nd-5th (default) blocks
    model = add_default_block(model, 64, init=initializer, regulizer_lambda=reg_lambda)
    model = add_default_block(model, 128, init=initializer, regulizer_lambda=reg_lambda)
    model = add_default_block(model, 256, init=initializer, regulizer_lambda=reg_lambda)
    model = add_default_block(model, 512, init=initializer, regulizer_lambda=reg_lambda)

    # LSTM output head
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(256, return_sequences=False, dropout=0.5))
    model.add(Dense(len(class_list), activation="softmax"))
    model.compile(
        loss="categorical_crossentropy", optimizer="Adam", metrics=["accuracy"]
    )
    # Avoid setting Batch_Size too high as you'll run out of RAM
    model.fit(
        x_train,
        y_train,
        epochs=400,
        # validation_data=(x_test,y_test),
        batch_size=32,
    )
    model.save(model_filename)
    return model


if __name__ == "__main__":
    main()
