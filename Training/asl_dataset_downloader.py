#!/bin/python3
"""
FILENAME :      asl_dataset_downloader.py

DESCRIPTION : Download all sign videos from Handspeak and store them into a dir with its associated english word.


Usage:
                        python3 asl_dataset_downloader.py


AUTHOR :   Network Silence        START DATE :    16 May 2021


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import requests
import re
import os
import uuid
from bs4 import BeautifulSoup

# Download all the Videos from https://www.handspeak.com/word/search/ and store them all in the ASL folder
# Make sure to only name them by the word


def create_folder(folder_name):
    """
    Create a folder in the asl dir I.e Name directory (word representation of the sign)
    :param folder_name: Folder to create
    :return:
    """
    if not os.path.exists(f"asl/{folder_name}"):
        os.mkdir(f"asl/{folder_name}")


def download_video(folder_name, soup):
    """
    Download all videos src into the folder_name
    :param folder_name: Location where we wanna store the video
    :param soup: Bs4 HTML object to fetch the video src from
    :return:
    """
    videos = soup.find_all("video")
    for video in videos:
        if video.has_attr("src"):
            video_link = requests.get(f"{tld}/{video['src']}")
            with open(f"asl/{folder_name}/{str(uuid.uuid4())}.mp4", "wb") as fd:
                fd.write(video_link.content)


def get_keyword(header):
    """
    Get word being represented from the header
    :param header: Header which contains the word the sign is representing
    :return: The Keyword being represented
    """
    rgx_list = ["<h1>ASL sign for ", "</h1>", "/.*", "'"]
    new_text = header
    for rgx_match in rgx_list:
        new_text = re.sub(rgx_match, "", new_text)
    print(new_text)
    return new_text


if __name__ == "__main__":
    id = 0
    tld = "https://www.handspeak.com"
    while True:
        id += 1
        next_page = requests.get(f"{tld}/word/search/index.php?id={id}").text
        new_soup = BeautifulSoup(next_page, "html.parser")
        word_header = str(new_soup.find("h1"))
        if word_header == "<h1>ASL Sign Language Dictionary - Search":
            break
        folder_name = get_keyword(word_header)
        create_folder(folder_name)
        download_video(folder_name, new_soup)
