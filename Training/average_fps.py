#!/bin/python3
"""
FILENAME :      average_fps.py

DESCRIPTION : Calculate the Average FPS of all videos in a work directory


Usage:
                        python3 average_fps.py


AUTHOR :   Network Silence        START DATE :    16 May 2021


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import os
import cv2

dir = "/media/null/Backup/asl/"


sum_frames = 0
video_count = 0

# Get Average FPS based on all videos we have downloaded
for folder in os.listdir(dir):
    for video in os.listdir(f"{dir}{folder}"):
        print(video)
        cap = cv2.VideoCapture(f"{dir}{folder}/{video}")
        sum_frames += float(cap.get(cv2.CAP_PROP_FPS))
        cap.release()
        video_count += 1

if video_count != 0:
    average = sum_frames / video_count
    print(f"Average FPS: {average}")
