#!/bin/python3
"""
FILENAME :      test_model.py

DESCRIPTION : Testing tool for the ASL model. Take every file in /test/ dir and run it against the model
                capture top 5 guesses and calculate how many time the correct guess appeared inside.

Usage:
                        python3 test_model.py


AUTHOR :   Network Silence        START DATE :    16 May 2021


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import os

from keras.engine.saving import load_model

from Lib.VideoCapture import VideoGet
import numpy as np
import operator
from Lib import Utilities as AslUtil


def main():
    """
    Control the flow of the program I.e start point
    :return:
    """
    training_data_path = "/media/null/Backup/test/"
    model_filename = f"/media/null/Repos/GitLab/ASL_Translator/trained_model_asl.hdf5"
    label_filename = f"/media/null/Repos/GitLab/ASL_Translator/model_labels.dat"
    image_size = 120
    frame_cap = 12

    model = load_model(model_filename)
    label_list = os.listdir(training_data_path)

    test_prediction(model, label_list, frame_cap, image_size, training_data_path)


def predict_sign(built_model, class_list, prediction_video_list, actual_word):
    """
    Based on the numpy array I.e prediction_video_list make a guess on what the english value is using the pretrained model
    :param built_model: Model we use to make prediction
    :param class_list: list of all possible words that we can predict
    :param prediction_video_list: Numpy array containing frame_cap amount of frame data
    :param actual_word: The Actual Word of the sign
    :return: Returns Bool based on if we found a match in top 5 or not
    """
    prediction_arr = np.squeeze(
        built_model.predict(np.expand_dims(prediction_video_list, axis=0)), axis=0
    )
    label_predictions = {}
    for i, label in enumerate(class_list):
        label_predictions[label] = prediction_arr[i]
    sorted_list = sorted(
        label_predictions.items(), key=operator.itemgetter(1), reverse=True
    )
    for i, class_prediction in enumerate(sorted_list):
        if i > 5 - 1 or class_prediction[1] == 0.0:
            break
        if class_prediction[0] == actual_word:
            return 1
    return 0


def test_prediction(built_model, class_list, max_frames, image_size, data_dir):
    """
    Test code for fetching video streams and of max_frame size and converting it to numpy arrays
    :param built_model: The Model to use for making predictions
    :param class_list: List of all the possible classes I.e words with sign representations we have
    :param max_frames: The Amount of Frames we want from each video
    :param image_size: The dimensions of the frame (Reformat to it)
    :param data_dir: The dir containing all the test video streams.
    :return:
    """
    prediction_video = []
    total_odds = 0
    for folder in os.listdir(data_dir):
        for video in os.listdir(f"{data_dir}{folder}"):
            try:
                cap = VideoGet(f"{data_dir}{folder}/{video}").start()
                prediction_video = []
                frame_count = 0
                while not cap.stopped and frame_count < max_frames:
                    image = cap.frame
                    frame_count += 1
                    prediction_video.append(AslUtil.image_processor(image, image_size))
                cap.stop()
            except Exception:
                print(f"Bugged File: {folder}/{video}")
                continue
            while len(prediction_video) > 0 and frame_count < max_frames:
                frame_count += 1
                prediction_video.append(prediction_video[-1])
        total_odds += predict_sign(built_model, class_list, prediction_video, folder)
    print(f"Accuracy top 5: {(total_odds / len(class_list)) * 100}")


if __name__ == "__main__":
    main()
