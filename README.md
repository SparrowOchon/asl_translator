# ASL To English
The following Repo is a custom trained CNN+LSTM Network capable of translating ASL to English.

## Features:
- Trained with over 10330 ASL Words (NOT JUST LETTERS)
- Works with Video and Camera feed
- 60% TOP 5 Accuracy
- Works independently of background, i.e., no black/white or green screen required.
- Threaded VideoCapture increasing cv2 capture speed by 10x

![Jolanta Lapiak](readme_link/trained_image.png)


![Jolanta Lapiak](readme_link/blurry_image.png)

The hand and Pose tracking even works during quick movements to accurately
sign gestures.


## Installation
Conda was used to manage dependencies as such:
- ```conda create --name asl_to_english --file requirements.txt``` 

## Usage
The `trained_model_asl.hdf5` is all that is required to download and use with your project. 
Check out the `test_prediction` function in `Complete/test_model.py` to view an example of the model in action.

## Questions:
### Why not just a CNN:
CNN's are beneficially when identifying changes in images, but this is not going to be accurate alone
when working with ASL because the changes occur through hand motions where sequence matters.
Two signs can involve similar hand positioning, and without accounting for sequence, there will be a severe increase in false positives.

### Why is the accuracy not Higher:
The main reason is there are only 1-2 unique videos for each word max, which was then flipped to account for the left-handed signers. So a total of four videos per word. As such, the main problem lacking for higher accuracy is datasets. (Note additional Tinkering with the NN could also help ). Adding Signsavvy would help significantly.

### Why no Threaded Video annotating
Unfortunately, `Mediapipe` objects are not thread-safe, meaning neither `multiprocessing` nor `multithreading` libraries will work. Asyncio would also not be helpful since CPU-bound operations are happening for each frame to annotate it. This adds
overhead making it slower than without.

### Training is too Slow or Crashes:
Since the model is already trained, this is not required. Assuming this is for another project or a custom edit. (PLEASE READ THE LICENSE). It can be
sped up using `generators` and increasing `batch_size.` The default 32 is tiny for the size of this dataset. For context, the 10330+ videos trained with the threaded video reader took `8` hours to train on a 1070ti and required `163GB` for a `128` batch size.


### Special Thanks
[Jolanta Lapiak and the HandSpeak Team](https://www.handspeak.com/word/search/index.php?id=4307) Huge thanks to Jolanta and the fantastic team at hand speak for putting together so many resources for those wanting to learn and researching sign language. Thank you so much for your tireless work.

### License
PLEASE CAREFULLY READ THIS LICENSE.

[Share don't Sell License](https://gitlab.com/SparrowOchon/show-dont-sell)
