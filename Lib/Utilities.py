#!/bin/python3
"""
FILENAME :      Utilities.py

DESCRIPTION : Mediapipe annotation utilities, responsible for applying mediapipe functions to files and images


AUTHOR :   Network Silence        START DATE :    16 May 2021


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import pickle

import cv2
import mediapipe as mp
import numpy as np

MEDIAPIPE_DRAWING = mp.solutions.drawing_utils
MEDIAPIPE_HOLISTIC = mp.solutions.holistic
HOLISTIC = MEDIAPIPE_HOLISTIC.Holistic(
    min_detection_confidence=0.5, min_tracking_confidence=0.6
)


def image_processor(video_frame, image_size):
    """
    Get Hand positions from video frame and annotate them on the image for training
    :param video_frame: The frame/image we want to annotate
    :param image_size: Dimension we want to resize the frame too
    :return annotated video_frame
    """
    video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)
    video_frame.flags.writeable = False
    results = HOLISTIC.process(video_frame)

    # Draw landmark annotation on the image.
    video_frame.flags.writeable = True
    MEDIAPIPE_DRAWING.draw_landmarks(
        video_frame, results.left_hand_landmarks, MEDIAPIPE_HOLISTIC.HAND_CONNECTIONS
    )
    MEDIAPIPE_DRAWING.draw_landmarks(
        video_frame, results.right_hand_landmarks, MEDIAPIPE_HOLISTIC.HAND_CONNECTIONS
    )
    MEDIAPIPE_DRAWING.draw_landmarks(
        video_frame, results.pose_landmarks, MEDIAPIPE_HOLISTIC.POSE_CONNECTIONS
    )
    video_frame = cv2.resize(video_frame, (image_size, image_size))
    video_frame = (video_frame / 255.0).astype(np.float32)

    return video_frame


def save_to_file(file_name, shape_to_save):
    with open(file_name, "wb") as f:
        pickle.dump(shape_to_save, f)
